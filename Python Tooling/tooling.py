from abc import ABCMeta, abstractmethod
from collections import deque
import heapq
from typing import Any, Generic, Iterable, List, Optional, Tuple, Type, TypeVar

_X = TypeVar('_X')

class Comparable(metaclass=ABCMeta):
    @abstractmethod
    def __lt__(self, other: Any) -> bool: ...
    
    @abstractmethod
    def __gt__(self, other: Any) -> bool: ...
_C = TypeVar('_C', bound=Comparable)

class ReverseComparable(Generic[_C]):
    _object: _C
    def __init__(self, object: _C):
        self._object = object
    
    def __lt__(self, other: _C) -> bool:
        return self._object.__gt__(other)
    
    def __gt__(self, other: _C) -> bool:
        return self._object.__lt__(other)
    
    def unwrap(self) -> _C:
        return self._object
_R = TypeVar('_R', bound=ReverseComparable)

class Queue(Generic[_X]):
    _data: deque[_X]
    
    def __init__(self, x: Type[_X], maxlen: Optional[int] = None):
        """Initialise Queue object

        Args:
            x (Type[_X]): Defines type of Queue Generic
            maxlen (Optional[int]): Maximum size of Queue. Defaults to None (unbounded).
        """
        self._data = deque(maxlen = maxlen)
    
    def __len__(self) -> int:    
        return len(self._data)
    
    @property
    def maxlen(self) -> Optional[int]:
        """Max length of queue (read-only)
        
        Returns:
            Optional[int]: Maximum size of Queue. None implies unbounded size.
        """        
        return self._data.maxlen
    
    def add(self, item: _X) -> None:
        """Enqueue operation
        
        Args:
            item (_X): Item to append to back of Queue.
        """        
        self._data.append(item)
    
    def pop(self) -> _X:
        """Dequeue operation

        Returns:
            _X: Item that was popped from front of Queue.
        """        
        return self._data.popleft()
    
    def peek(self) -> _X:
        """Peek operation

        Returns:
            _X: View of Item at front of Queue.
        """        
        return self._data[0]
    
    def clear(self) -> None:
        """Clear operation
        """        
        self._data.clear()
    
    def extend(self, iterable: Iterable[_X]) -> None:
        """Enqueue operation (multiple Items)

        Args:
            iterable (Iterable[_X]): Iterable of Items to append to end of Queue.
        """        
        self._data.extend(iterable)
    
    def index(self, item: _X, start: int = 0, stop: int = ...) -> int:
        """Search operation

        Args:
            item (_X): Item to search for.
            start (Optional[int]): Index to start searching at (inclusive). Defaults to 0.
            stop (Optional[int]): Index to end searching at (exclusive). Defaults to end of Queue.

        Returns:
            int: Index of left-most occurrence of search Item. Returns -1 if not found.
        """        
        if stop is Ellipsis:
            stop = len(self._data)
        
        try:
            return self._data.index(item, start, stop)
        except ValueError:
            return -1
    
    def reverse(self) -> None:
        """In-place reverse operation
        """        
        self._data.reverse()
    
    def is_empty(self) -> bool:
        """IsEmpty operation

        Returns:
            bool: Boolean that represents if there is no Items in the Queue.
        """        
        return len(self._data) == 0
    
    def is_full(self) -> bool:
        """IsFull operation

        Returns:
            bool: Boolean that represents if the Queue will return an error if any further Items are enqueued.
        """        
        return self._data.maxlen is None or self._data.maxlen == len(self._data)
    
    def count(self, item: _X) -> int:
        """Counting search operation

        Args:
            item (_X): Item to search for.

        Returns:
            int: Count of the number of Queue elements that match search item.
        """        
        return self._data.count(item)

class Stack(Generic[_X]):
    _data: deque[_X]
    
    def __init__(self, t: Type[_X], maxlen: Optional[int] = None):
        """Initialise Stack object

        Args:
            t (Type[_X]): Defines type of Stack Generic
            maxlen (Optional[int]): Maximum size of Stack. Defaults to None (unbounded).
        """
        self._data = deque(maxlen = maxlen)
    
    def __len__(self) -> int:    
        return len(self._data)
    
    @property
    def maxlen(self) -> Optional[int]:
        """Max length of queue (read-only)
        
        Returns:
            Optional[int]: Maximum size of Stack. None implies unbounded size.
        """        
        return self._data.maxlen
    
    def push(self, item: _X) -> None:
        """Push operation
        
        Args:
            item (_X): Item to push onto top of Stack.
        """        
        self._data.append(item)
    
    def pop(self) -> _X:
        """Pop operation

        Returns:
            _X: Item that was popped from top of Stack.
        """        
        return self._data.pop()
    
    def peek(self) -> _X:
        """Peek operation

        Returns:
            _X: View of Item on top of Stack.
        """        
        return self._data[0]
    
    def clear(self) -> None:
        """Clear operation
        """        
        self._data.clear()
    
    def extend(self, iterable: Iterable[_X]) -> None:
        """Push operation (multiple Items)

        Args:
            iterable (Iterable[_X]): Iterable of Items to push onto top of Stack.
        """        
        self._data.extend(iterable)
    
    def index(self, item: _X, start: int = 0, stop: int = ...) -> int:
        """Search operation

        Args:
            item (_X): Item to search for.
            start (Optional[int]): Index to start searching at (inclusive). Defaults to 0.
            stop (Optional[int]): Index to end searching at (exclusive). Defaults to end of Stack.

        Returns:
            int: Index of left-most occurrence of search Item. Returns -1 if not found.
        """
        if stop is Ellipsis:
            stop = len(self._data)
        
        try:
            return self._data.index(item, start, stop)
        except ValueError:
            return -1
    
    def reverse(self) -> None:
        """In-place reverse operation
        """        
        self._data.reverse()
    
    def is_empty(self) -> bool:
        """IsEmpty operation

        Returns:
            bool: Boolean that represents if there is no Items on the Stack.
        """        
        return len(self._data) == 0
    
    def is_full(self) -> bool:
        """IsFull operation

        Returns:
            bool: Boolean that represents if the Stack will return an error if any further Items are pushed.
        """        
        return self._data.maxlen is None or self._data.maxlen == len(self._data)
    
    def count(self, item: _X) -> int:
        """Counting search operation

        Args:
            item (_X): Item to search for.

        Returns:
            int: Count of the number of Stack elements that match search item.
        """        
        return self._data.count(item)

class StableMaxHeap(Generic[_C, _X]):
    _data: List[Tuple[ReverseComparable[_C], int, _X]]
    _entry_count: int
    _max_size: Optional[int]
    _max_entry_count: int
    
    def __init__(self, keyType: Type[_C], valueType: Type[_X], max_size: Optional[int] = None, max_entry_count: int = 10000, initial_data: Optional[Iterable[Tuple[_C, _X]]] = None):
        """_summary_

        Args:
            keyType (Type[_C]): Type of the key used when sorting Items in the StableMaxHeap.
            valueType (Type[_X]): Type of the Items in the StableMaxHeap.
            max_size (Optional[int]): Maximum size of StableMaxHeap. Defaults to None (unbounded).
            max_entry_count (Optional[int]): Maximum number of insertions before rebalancing insertion order index. Defaults to 10000.
            initial_data (Optional[List[Tuple[_C, _X]]]): Initial contents for the StableMaxHeap. Defaults to None.
        """
        self._entry_count = 0
        self._max_size = max_size
        self._max_entry_count = max_entry_count
        self.data = []
        if initial_data is not None:
            self.extend(initial_data)
    
    def __len__(self) -> int:    
        return len(self._data)
    
    def _get_entry_count(self) -> int:
        self._entry_count += 1
        return self._entry_count - 1
    
    def _rebalance_entries(self) -> None:
        entry_counts = [(i, self._data[i][1]) for i in range(len(self._data))]
        entry_counts.sort(key=lambda x: x[1])
        for i in range(len(entry_counts)):
            self._data[i] = (self._data[i][0], i, self._data[i][2])
        self._entry_count = len(entry_counts)
    
    def push(self, priority: _C, item: _X) -> None:
        """Push operation
        
        Args:
            priority (_C): Key to use when sorting the item in the StableMaxHeap.
            item (_X): Item to add to the StableMaxHeap.
        """
        if(self._max_size is not None and len(self._data) >= self._max_size):
            raise OverflowError("Attempted to push onto StableMaxHeap when at capacity.")
        
        heapq.heappush(self._data, (ReverseComparable(priority), self._get_entry_count(), item))
        
        if(self._entry_count >= self._max_entry_count):
            self._rebalance_entries()
        if(self._entry_count >= self._max_entry_count):
            self._max_entry_count = self._max_entry_count * 3 // 2
    
    def pop(self) -> Tuple[_C, _X]:
        """Pop operation

        Returns:
            Tuple[_C,_X]: Priority and Item that was popped from the top of the StableMaxHeap.
        """
        x, _, y = heapq.heappop(self._data)
        return (x.unwrap(), y)
    
    def peek(self) -> Tuple[_C, _X]:
        """Peek operation

        Returns:
            Tuple[_C,_X]: View of Priority and Item on top of the StableMaxHeap.
        """        
        x, _, y = self._data[0]
        return (x.unwrap(), y)
    
    def clear(self) -> None:
        """Clear operation
        """        
        self._data = []
    
    def extend(self, iterable: Iterable[Tuple[_C, _X]]) -> None:
        """Push operation (multiple Items)

        Args:
            iterable (Iterable[Tuple[_C,_X]]): Iterable of Priorities and Items to push onto the StableMaxHeap.
        """
        new_iterable = map(lambda x: (ReverseComparable(x[0]), self._get_entry_count(), x[1]), iterable)
        self._data.extend(new_iterable)
        
        if(self._max_size is not None and len(self._data) >= self._max_size):
            raise OverflowError("Attempted to push onto StableMaxHeap when at capacity.")
        
        heapq.heapify(self._data)
        
        if(self._entry_count >= self._max_entry_count):
            self._rebalance_entries()
        if(self._entry_count >= self._max_entry_count):
            self._max_entry_count = self._max_entry_count * 3 // 2
    
    def is_empty(self) -> bool:
        """IsEmpty operation

        Returns:
            bool: Boolean that represents if there is no Items in the StableMaxHeap.
        """        
        return len(self._data) == 0
    
    def is_full(self) -> bool:
        """IsFull operation

        Returns:
            bool: Boolean that represents if the StableMaxHeap will return an error if any further Items are pushed.
        """        
        return self._max_size is not None and len(self._data) >= self._max_size

class StableMinHeap(Generic[_C, _X]):
    _data: List[Tuple[_C, int, _X]]
    _entry_count: int
    _max_size: Optional[int]
    _max_entry_count: int
    
    def __init__(self, keyType: Type[_C], valueType: Type[_X], max_size: Optional[int] = None, max_entry_count: int = 10000, initial_data: Optional[Iterable[Tuple[_C, _X]]] = None):
        """_summary_

        Args:
            keyType (Type[_C]): Type of the key used when sorting Items in the StableMinHeap.
            valueType (Type[_X]): Type of the Items in the StableMinHeap.
            max_size (Optional[int]): Maximum size of StableMinHeap. Defaults to None (unbounded).
            max_entry_count (Optional[int]): Maximum number of insertions before rebalancing insertion order index. Defaults to 10000.
            initial_data (Optional[List[Tuple[_C, _X]]]): Initial contents for the StableMinHeap. Defaults to None.
        """
        self._entry_count = 0
        self._max_size = max_size
        self._max_entry_count = max_entry_count
        self.data = []
        if initial_data is not None:
            self.extend(initial_data)
    
    def __len__(self) -> int:    
        return len(self._data)
    
    def _get_entry_count(self) -> int:
        self._entry_count += 1
        return self._entry_count - 1
    
    def _rebalance_entries(self) -> None:
        entry_counts = [(i, self._data[i][1]) for i in range(len(self._data))]
        entry_counts.sort(key=lambda x: x[1])
        for i in range(len(entry_counts)):
            self._data[i] = (self._data[i][0], i, self._data[i][2])
        self._entry_count = len(entry_counts)
    
    def push(self, priority: _C, item: _X) -> None:
        """Push operation
        
        Args:
            priority (_C): Key to use when sorting the item in the StableMinHeap.
            item (_X): Item to add to the StableMinHeap.
        """
        if(self._max_size is not None and len(self._data) >= self._max_size):
            raise OverflowError("Attempted to push onto StableMinHeap when at capacity.")
        
        heapq.heappush(self._data, (priority, self._get_entry_count(), item))
        
        if(self._entry_count >= self._max_entry_count):
            self._rebalance_entries()
        if(self._entry_count >= self._max_entry_count):
            self._max_entry_count = self._max_entry_count * 3 // 2
    
    def pop(self) -> Tuple[_C, _X]:
        """Pop operation

        Returns:
            Tuple[_C,_X]: Priority and Item that was popped from the top of the StableMinHeap.
        """
        x, _, y = heapq.heappop(self._data)
        return (x, y)
    
    def peek(self) -> Tuple[_C, _X]:
        """Peek operation

        Returns:
            Tuple[_C,_X]: View of Priority and Item on top of the StableMinHeap.
        """        
        x, _, y = self._data[0]
        return (x, y)
    
    def clear(self) -> None:
        """Clear operation
        """        
        self._data = []
    
    def extend(self, iterable: Iterable[Tuple[_C, _X]]) -> None:
        """Push operation (multiple Items)

        Args:
            iterable (Iterable[Tuple[_C,_X]]): Iterable of Priorities and Items to push onto the StableMinHeap.
        """
        new_iterable = map(lambda x: (x[0], self._get_entry_count(), x[1]), iterable)
        self._data.extend(new_iterable)
        
        if(self._max_size is not None and len(self._data) >= self._max_size):
            raise OverflowError("Attempted to push onto StableMinHeap when at capacity.")
        
        heapq.heapify(self._data)
        
        if(self._entry_count >= self._max_entry_count):
            self._rebalance_entries()
        if(self._entry_count >= self._max_entry_count):
            self._max_entry_count = self._max_entry_count * 3 // 2
    
    def is_empty(self) -> bool:
        """IsEmpty operation

        Returns:
            bool: Boolean that represents if there is no Items in the StableMinHeap.
        """        
        return len(self._data) == 0
    
    def is_full(self) -> bool:
        """IsFull operation

        Returns:
            bool: Boolean that represents if the StableMinHeap will return an error if any further Items are pushed.
        """        
        return self._max_size is not None and len(self._data) >= self._max_size

class MaxHeap(Generic[_C, _X]):
    _data: List[Tuple[ReverseComparable[_C], _X]]
    _max_size: Optional[int]
    
    def __init__(self, keyType: Type[_C], valueType: Type[_X], max_size: Optional[int] = None, initial_data: Optional[Iterable[Tuple[_C, _X]]] = None):
        """_summary_

        Args:
            keyType (Type[_C]): Type of the key used when sorting Items in the MaxHeap.
            valueType (Type[_X]): Type of the Items in the MaxHeap
            max_size (Optional[int]): Maximum size of MaxHeap. Defaults to None (unbounded).
            initial_data (Optional[List[Tuple[_C, _X]]]): Initial contents for the MaxHeap. Defaults to None.
        """
        self._max_size = max_size
        self.data = []
        if initial_data is not None:
            self.extend(initial_data)
    
    def __len__(self) -> int:    
        return len(self._data)
    
    def push(self, priority: _C, item: _X) -> None:
        """Push operation
        
        Args:
            priority (_C): Key to use when sorting the Item in the MaxHeap.
            item (_X): Item to add to the MaxHeap.
        """
        if(self._max_size is not None and len(self._data) >= self._max_size):
            raise OverflowError("Attempted to push onto MaxHeap when at capacity.")
        
        heapq.heappush(self._data, (ReverseComparable(priority), item))
    
    def pop(self) -> Tuple[_C, _X]:
        """Pop operation

        Returns:
            Tuple[_C,_X]: Priority and Item that was popped from the top of the MaxHeap.
        """
        x, y = heapq.heappop(self._data)
        return (x.unwrap(), y)
    
    def peek(self) -> Tuple[_C, _X]:
        """Peek operation

        Returns:
            Tuple[_C,_X]: View of Priority and Item on top of the MaxHeap.
        """        
        x, y = self._data[0]
        return (x.unwrap(), y)
    
    def clear(self) -> None:
        """Clear operation
        """        
        self._data = []
    
    def extend(self, iterable: Iterable[Tuple[_C, _X]]) -> None:
        """Push operation (multiple Items)

        Args:
            iterable (Iterable[Tuple[_C,_X]]): Iterable of Priorities and Items to push onto the MaxHeap.
        """
        new_iterable = map(lambda x: (ReverseComparable(x[0]), x[1]), iterable)
        self._data.extend(new_iterable)
        
        if(self._max_size is not None and len(self._data) > self._max_size):
            raise OverflowError("Attempted to push onto MaxHeap when at capacity.")
        
        heapq.heapify(self._data)
    
    def is_empty(self) -> bool:
        """IsEmpty operation

        Returns:
            bool: Boolean that represents if there is no Items in the MaxHeap.
        """        
        return len(self._data) == 0
    
    def is_full(self) -> bool:
        """IsFull operation

        Returns:
            bool: Boolean that represents if the MaxHeap will return an error if any further Items are pushed.
        """        
        return self._max_size is not None and len(self._data) >= self._max_size

class MinHeap(Generic[_C, _X]):
    _data: List[Tuple[_C, _X]]
    _max_size: Optional[int]
    
    def __init__(self, keyType: Type[_C], valueType: Type[_X], max_size: Optional[int] = None, initial_data: Optional[Iterable[Tuple[_C, _X]]] = None):
        """_summary_

        Args:
            keyType (Type[_C]): Type of the key used when sorting Items in the MaxHeap.
            valueType (Type[_X]): Type of the Items in the MaxHeap
            max_size (Optional[int]): Maximum size of StableMaxHeap. Defaults to None (unbounded).
            initial_data (Optional[List[Tuple[_C, _X]]]): Initial contents for the MaxHeap. Defaults to None.
        """
        self._max_size = max_size
        self.data = []
        if initial_data is not None:
            self.extend(initial_data)
    
    def __len__(self) -> int:    
        return len(self._data)
    
    def push(self, priority: _C, item: _X) -> None:
        """Push operation
        
        Args:
            priority (_C): Key to use when sorting the Item in the MaxHeap.
            item (_X): Item to add to the MaxHeap.
        """
        if(self._max_size is not None and len(self._data) >= self._max_size):
            raise OverflowError("Attempted to push onto MaxHeap when at capacity.")
        
        heapq.heappush(self._data, (priority, item))
    
    def pop(self) -> Tuple[_C, _X]:
        """Pop operation

        Returns:
            Tuple[_C,_X]: Priority and Item that was popped from the top of the MaxHeap.
        """
        return heapq.heappop(self._data)
    
    def peek(self) -> Tuple[_C, _X]:
        """Peek operation

        Returns:
            Tuple[_C,_X]: View of Priority and Item on top of the MaxHeap.
        """
        return self._data[0]
    
    def clear(self) -> None:
        """Clear operation
        """        
        self._data = []
    
    def extend(self, iterable: Iterable[Tuple[_C, _X]]) -> None:
        """Push operation (multiple Items)

        Args:
            iterable (Iterable[Tuple[_C,_X]]): Iterable of Priorities and Items to push onto the MaxHeap.
        """
        self._data.extend(iterable)
        
        if(self._max_size is not None and len(self._data) > self._max_size):
            raise OverflowError("Attempted to push onto MaxHeap when at capacity.")
        
        heapq.heapify(self._data)
    
    def is_empty(self) -> bool:
        """IsEmpty operation

        Returns:
            bool: Boolean that represents if there is no Items in the MaxHeap.
        """        
        return len(self._data) == 0
    
    def is_full(self) -> bool:
        """IsFull operation

        Returns:
            bool: Boolean that represents if the MaxHeap will return an error if any further Items are pushed.
        """        
        return self._max_size is not None and len(self._data) >= self._max_size   