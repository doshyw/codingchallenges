#!/bin/python3

import math
from typing import Callable, Collection, Optional, TypeVar

palindrome_list = []

def is_palindrome(n: int):
    num_digits = math.floor(math.log10(n)) + 1
    
    for a in range(math.floor(num_digits / 2)):
        b = num_digits - a - 1
        if (n // pow(10, a)) % 10 != (n // pow(10, b)) % 10:
            return False
    
    return True

X = TypeVar('X')
Y = TypeVar('Y')
def max_below(arr: Collection[X], search: Y,  comp_func: Optional[Callable[[Y, Y], int]] = lambda x, y: 1 if x > y else 0 if x == y else -1, map_func: Optional[Callable[[X], Y]] = lambda x: x) -> int:
    L = 0
    R = len(arr)
    while L < R:
        m = (L + R) // 2
        val = map_func(arr[m])
        if comp_func(val, search) == -1:
            L = m + 1
        else:
            R = m
    return L - 1

for i in range(101, 1000):
    for j in range(i, 1000):
        if i < j:
            new_num = i*j
            if(new_num >= 101101 and is_palindrome(new_num) and new_num not in palindrome_list):
                palindrome_list.append(new_num)

palindrome_list.sort()

t = 1#int(input().strip())
for a0 in range(t):
    n = 101102#int(input().strip())
    
    index = max_below(palindrome_list, n)
    if n == -1:
        raise RuntimeError(f'Input value n = {n} out of range')
    print(palindrome_list)
    print(index)
    print(palindrome_list[index])