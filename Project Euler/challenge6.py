#!/bin/python3

def sum_square(n: int) -> int:
    return (n*(n+1)*(2*n+1)) // 6

def square_sum(n: int) -> int:
    return pow((n*(n+1)) // 2, 2)

t = int(input().strip())
for a0 in range(t):
    n = int(input().strip())
    
    print(square_sum(n) - sum_square(n))