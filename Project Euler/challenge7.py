#!/bin/python3

import math

MAX_PRIME = 104729

current_factor = 1
max_factor = int(math.sqrt(MAX_PRIME))
prime_set = []
is_prime = [True] * (MAX_PRIME + 1)
is_prime[0] = False
is_prime[1] = False

# https://algotree.org/algorithms/numeric/prime_sieve/
def calc_prime(n: int) -> int:
    global current_factor, max_factor, prime_set, is_prime
    while current_factor < max_factor:
        if len(prime_set) >= n:
            return prime_set[n - 1]
        if is_prime[current_factor]:
            for i in range(pow(current_factor, 2), MAX_PRIME, current_factor):
                is_prime[i] = False
            prime_set.append(current_factor)
        current_factor += 1
    while current_factor <= MAX_PRIME:
        if len(prime_set) >= n:
            return prime_set[n - 1]
        if is_prime[current_factor]:
            prime_set.append(current_factor)
        current_factor += 1
    return prime_set[n - 1]
    

t = int(input().strip())
for a0 in range(t):
    n = int(input().strip())
    
    print(calc_prime(n))