#!/bin/python3

import math

MAX_N = 3000

r = 2
N_table = [0] * MAX_N

# https://en.wikipedia.org/wiki/Formulas_for_generating_Pythagorean_triples#Dickson's_method
t = int(input().strip())
for a0 in range(t):
    n = int(input().strip())
    
    # Maximum value of r such that N = a + b + c holds
    max_r = n / (3 + 2*math.sqrt(2))
    
    while r <= max_r:
        prod = pow(r,2) // 2
        for s in range(1, math.floor(math.sqrt(prod)) + 1):
            if prod % s == 0:
                t = (prod // s)
                calc_n = 3*r + 2*s + 2*t
                if calc_n <= MAX_N:
                    val = (r + s) * (r + t) * (r + s + t)
                    if N_table[calc_n - 1] < val:
                        N_table[calc_n - 1] = val
        r += 2
    
    if N_table[n - 1] == 0:
        print(-1)
    else:
        print(N_table[n - 1])