#!/bin/python3

t = int(input().strip())
for a0 in range(t):
    n,k = input().strip().split(' ')
    n,k = [int(n),int(k)]
    num = input().strip()
    
    digits = [0] * n
    for i in range(n):
        digits[i] = int(num[i])
    
    max_prod = 0
    s = 0
    e = 0
    prod = 1
    while e < n:
        if digits[e] == 0:
            s = e + 1
            e = s
            prod = 1
            continue
        prod *= digits[e]
        if e == s + k - 1:
            if prod > max_prod:
                max_prod = prod
            prod //= digits[s]
            s += 1
        e += 1
    
    print(max_prod)