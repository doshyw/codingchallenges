#!/bin/python3

def n(k, N):
    return (N-1) // k

def S(k, N):
    val = n(k, N)
    return (k*val*(val+1)) // 2
    
t = int(input().strip())
for a0 in range(t):
    N = int(input().strip())
    print(S(3, N) + S(5, N) - S(15, N))
