#!/bin/python3

import math

minimal_factors = [0] * 40
minimal_factors[0] = 1

def calc_factors(n: int) -> int:
    n_reduce = n
    for i in range(1, n - 1):
        if minimal_factors[i] == 0:
            calc_factors(i + 1)
        if n_reduce % minimal_factors[i] == 0:
            n_reduce //= minimal_factors[i]
    minimal_factors[n - 1] = n_reduce

t = int(input().strip())
for a0 in range(t):
    n = int(input().strip())
    calc_factors(n)
    print(math.prod(minimal_factors[:n]))