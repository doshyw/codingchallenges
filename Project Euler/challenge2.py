#!/bin/python3

import math

PHI = (math.sqrt(5) + 1.0) / 2.0

# Array for memoization
F = [0] * 100

def fibonacci(n):
    if n < 1:
        return 0
    elif n < 3:
        F[n] = 1
        return F[n]
    
    if F[n]:
        return F[n]
    
    if n & 1:
        k = (n + 1) // 2
        F[n] = fibonacci(k) * fibonacci(k) + fibonacci(k-1) * fibonacci(k-1)
    else:
        k = n // 2
        F[n] = (2*fibonacci(k-1) + fibonacci(k)) * fibonacci(k)
    
    return F[n]

t = int(input().strip())
for a0 in range(t):
    F_bound = int(input().strip())
    
    # Calculate n such that F_{n+1} > F_bound and F_n <= F_bound
    n_approx = math.log(math.sqrt(5)*F_bound) / math.log(PHI)
    n_floor  = math.floor(n_approx)
    F_approx = round(math.pow(PHI, n_floor)     / math.sqrt(5))
    F_upper  = round(math.pow(PHI, n_floor + 1) / math.sqrt(5))
    
    if F_upper > F_bound and F_approx <= F_bound:
        n = n_floor
    else:
        n = n_floor + 1
    
    i = 3
    acc = 0
    while i <= n:
        acc += fibonacci(i)
        i += 3
    
    print(acc)