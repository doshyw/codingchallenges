#!/bin/python3

import math
from typing import List, Tuple

WITNESSES_10    = [2]
WITNESSES_23    = [31, 73]
WITNESSES_SHORT = [2, 7, 61]
WITNESSES_40    = [2, 3, 5, 7, 11]
WITNESSES_41    = [2, 3, 5, 7, 11, 13]
WITNESSES_48    = [2, 3, 5, 7, 11, 13, 17]
WITNESSES_61    = [2, 3, 5, 7, 11, 13, 17, 19, 23]
WITNESSES_LONG  = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37]
WITNESSES_81    = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41]

MAX_10    = 2047
MAX_23    = 9080190
MAX_SHORT = 4759123140
MAX_40    = 2152302898746
MAX_41    = 3474749660382
MAX_48    = 341550071728320
MAX_61    = 3825123056546413050
MAX_LONG  = 318665857834031151167460
MAX_81    = 3317044064679887385961980

# https://en.wikipedia.org/wiki/Miller-Rabin_primality_test#Testing_against_small_sets_of_bases
def primality_test(n: int) -> bool:
    def deterministic_miller_rabin(n: int, witnesses: List[int]) -> bool:
        if n < 2:
            return False
        elif n == 2:
            return True
        elif n % 2 == 0:
            return False
        
        r = 0
        d = n - 1
        while d % 2 == 0:
            d = d // 2
            r += 1
        
        for a in witnesses:
            x = pow(a, d, n)
            if(x == 1 or x == n - 1):
                continue
            cont = False
            for j in range(r - 1):
                x = pow(x, 2, n)
                if x == n - 1:
                    cont = True
                    break
            if cont:
                continue
            else:
                return False
        return True
    
    if n <= MAX_10:
        return deterministic_miller_rabin(n, WITNESSES_10)
    elif n <= MAX_23:
        return deterministic_miller_rabin(n, WITNESSES_23)
    elif n <= MAX_SHORT:
        return deterministic_miller_rabin(n, WITNESSES_SHORT)
    elif n <= MAX_40:
        return deterministic_miller_rabin(n, WITNESSES_40)
    elif n <= MAX_41:
        return deterministic_miller_rabin(n, WITNESSES_41)
    elif n <= MAX_48:
        return deterministic_miller_rabin(n, WITNESSES_48)
    elif n <= MAX_61:
        return deterministic_miller_rabin(n, WITNESSES_61)
    elif n <= MAX_LONG:
        return deterministic_miller_rabin(n, WITNESSES_LONG)
    elif n <= MAX_81:
        return deterministic_miller_rabin(n, WITNESSES_81)
    else:
        raise RuntimeError(f'Insufficient set of witnesses to test n = {n}')
    

t = int(input().strip())
for a0 in range(t):
    n = int(input().strip())
    
    if n == 1:
        print(1)
        continue
    
    while n % 2 == 0:
        n = n // 2
    
    if n == 1:
        print(2)
        continue
    
    factor = 3
    do_loop = not primality_test(n)
    while do_loop:
        if n % factor == 0:
            n = n // factor
            if primality_test(n):
                do_loop = False
        else:
            factor += 2
    
    print(n)