import math
from typing import Callable, Collection, List, Optional, Tuple, TypeVar, Union

EPS = 1e-12
X = TypeVar('X')
Y = TypeVar('Y')

# Makes a deep copy of elements from a source array into a destination array.
# Allows an optional 'n' parameter, which defines the number of elements to copy
#   (by default, the function will try to copy all of the source elements).
def array_copy(dest: List[X], src: List[X], n: Optional[int]) -> None:
    dest_len = len(dest)
    dest_src = n if n is not None else len(src)
    
    if dest_len < dest_src:
        RuntimeError("Destination array is too small to receive source elements.")
        
    for i in range(dest_src):
        dest[i] = src[i]
        
# Performs a bisection search for 'search' in 'arr' for generic types. For multiple
#   successful items in 'arr', returns the index of the left-most item.
# Allows an optional comparison function, where 'comp_func' must return -1, 0, or 1
#   based on the relative ordering of the search and arr objects. By default, it 
#   simply uses arr > search -> -1, arr == search -> 0, arr < search -> 1.
# Also allows an optional map function, to allow 'arr' values to be mapped to the
#    same type as 'search' before the comparison is applied.
def bisection(arr: Collection[X], search: Y,  comp_func: Callable[[Y, Y], int] = lambda x, y: 1 if x > y else 0 if x == y else -1, map_func: Callable[[X], Y] = lambda x: x) -> int:
    L = 0
    R = len(arr)
    while(L < R):
        m = (L + R) // 2
        val = map_func(arr[m])
        if(comp_func(val, search) == -1):
            L = m + 1
        else:
            R = m
    if(L < len(arr) and comp_func(map_func(arr[L]), search) == 0):
        return L
    else:
        return -1


# Calculates the rank of the search array (the index of the largest value smaller than the search value).
# If there are no values smaller than the search value in the array, returns -1.
def max_below(arr: Collection[X], search: Y, inclusive: bool = True, comp_func: Callable[[Y, Y], int] = lambda x, y: 1 if x > y else 0 if x == y else -1, map_func: Callable[[X], Y] = lambda x: x) -> int:
    L = 0
    R = len(arr)
    while L < R:
        m = (L + R) // 2
        val = map_func(arr[m])
        if comp_func(val, search) == -1:
            L = m + 1
        else:
            R = m
    if L == len(arr):
        return L
    elif inclusive and map_func(arr[L]) == search:
        return L + 1
    else:
        return L


# Calculates the rank of the search array (the index of the smallest value larger than the search value).
# If there are no values larger than the search value in the array, returns -1.
def min_above(arr: Collection[X], search: Y, inclusive: bool = True, comp_func: Optional[Callable[[Y, Y], int]] = lambda x, y: 1 if x > y else 0 if x == y else -1, map_func: Optional[Callable[[X], Y]] = lambda x: x) -> int:
    L = 0
    R = len(arr)
    while L < R:
        m = (L + R) // 2
        val = map_func(arr[m])
        if comp_func(val, search) == 1:
            R = m
        else:
            L = m + 1
    if R == len(arr):
        return R
    elif inclusive and map_func(arr[R - 1]) == search:
        return R - 1
    else:
        return R


# Determines whether a Collection is correctly unpacked from a containing Tuple or List,
#   and unpacks it if not.
# Useful for when returning Collections from Dictionaries.
def unpack_tuple(obj: Union[List[X], Tuple[List[X]], List[List[X]]]) -> List[X]:
    if len(obj) == 1 and type(obj[0]) is list:
        return obj[0]
    else:
        return obj

WITNESSES_10    = [2]
WITNESSES_23    = [31, 73]
WITNESSES_SHORT = [2, 7, 61]
WITNESSES_40    = [2, 3, 5, 7, 11]
WITNESSES_41    = [2, 3, 5, 7, 11, 13]
WITNESSES_48    = [2, 3, 5, 7, 11, 13, 17]
WITNESSES_61    = [2, 3, 5, 7, 11, 13, 17, 19, 23]
WITNESSES_LONG  = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37]
WITNESSES_81    = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41]

MAX_10    = 2047
MAX_23    = 9080190
MAX_SHORT = 4759123140
MAX_40    = 2152302898746
MAX_41    = 3474749660382
MAX_48    = 341550071728320
MAX_61    = 3825123056546413050
MAX_LONG  = 318665857834031151167460
MAX_81    = 3317044064679887385961980

# https://en.wikipedia.org/wiki/Miller-Rabin_primality_test#Testing_against_small_sets_of_bases
def primality_test(n: int) -> bool:
    def deterministic_miller_rabin(n: int, witnesses: List[int]) -> bool:
        if n < 2:
            return False
        elif n == 2:
            return True
        elif n % 2 == 0:
            return False
        
        r = 0
        d = n - 1
        while d % 2 == 0:
            d = d // 2
            r += 1
        
        for a in witnesses:
            x = pow(a, d, n)
            if(x == 1 or x == n - 1):
                continue
            cont = False
            for j in range(r - 1):
                x = pow(x, 2, n)
                if x == n - 1:
                    cont = True
                    break
            if cont:
                continue
            else:
                return False
        return True
    
    if n <= MAX_10:
        return deterministic_miller_rabin(n, WITNESSES_10)
    elif n <= MAX_23:
        return deterministic_miller_rabin(n, WITNESSES_23)
    elif n <= MAX_SHORT:
        return deterministic_miller_rabin(n, WITNESSES_SHORT)
    elif n <= MAX_40:
        return deterministic_miller_rabin(n, WITNESSES_40)
    elif n <= MAX_41:
        return deterministic_miller_rabin(n, WITNESSES_41)
    elif n <= MAX_48:
        return deterministic_miller_rabin(n, WITNESSES_48)
    elif n <= MAX_61:
        return deterministic_miller_rabin(n, WITNESSES_61)
    elif n <= MAX_LONG:
        return deterministic_miller_rabin(n, WITNESSES_LONG)
    elif n <= MAX_81:
        return deterministic_miller_rabin(n, WITNESSES_81)
    else:
        raise RuntimeError(f'Insufficient set of witnesses to test n = {n}')


def check_intersect(point1: Tuple[int, int], point2: Tuple[int, int], point3: Tuple[int, int], point4: Tuple[int, int]):
    class Point:
        def __init__(self, x: float, y: float):
            self.x: float = x
            self.y: float = y
        
        def __lt__(self, other: 'Point'):
            return (self.x < other.x - EPS) or (abs(self.x - other.x) < EPS and self.y < other.y - EPS)
    
    class Line:
        def __init__(self, p: Point, q: Point):
            self.a = p.y - q.y
            self.b = q.x - p.x
            self.c = -self.a * p.x - self.b * p.y
            self.norm()
        
        def norm(self):
            z = math.sqrt(self.a * self.a + self.b * self.b)
            if abs(z) > EPS:
                self.a /= z
                self.b /= z
                self.c /= z
        
        def dist(self, p: Point) -> float:
            return self.a * p.x + self.b * p.y + self.c
    
    def det_2d(a: float, b: float, c: float, d: float) -> float:
        return a * d - b * c
    
    def between(l: float, r: float, x: float) -> bool:
        return (min(l, r) <= x + EPS) and (x <= max(l,r) + EPS)
    
    def intersect_1d(a: float, b: float, c: float, d: float) -> bool:
        if a > b:
            tmp = a
            a = b
            b = tmp
        if c > d:
            tmp = c
            c = d
            d = tmp
        return max(a,c) <= min(b,d) + EPS
    
    def intersect_2d(a: Point, b: Point, c: Point, d: Point) -> Tuple[bool, Point, Point]:
        if not (intersect_1d(a.x, b.x, c.x, d.x) and intersect_1d(a.y, b.y, c.y, d.y)):
            return (False, None, None)
        
        m = Line(a, b)
        n = Line(c, d)
        zn = det_2d(m.a, m.b, n.a, n.b)
        if abs(zn) < EPS:
            if abs(m.dist(c)) > EPS or abs(n.dist(a)) > EPS:
                return (False, None, None)
            if b < a:
                tmp = a
                a = b
                b = tmp
            if d < c:
                tmp = c
                c = d
                d = tmp
            return (True, max(a, c), min(b, d))
        else:
            x = -det_2d(m.c, m.b, n.c, n.b) / zn
            y = -det_2d(m.a, m.c, n.a, n.c) / zn
            return_bool = between(a.x, b.x, x) and between(a.y, b.y, y) and between(c.x, d.x, x) and between(c.y, d.y, y)
            return (return_bool, Point(x, y), Point(x, y))
    
    return_val = intersect_2d(Point(point1[0], point1[1]), Point(point2[0], point2[1]), Point(point3[0], point3[1]), Point(point4[0], point4[1]))
    return return_val[0]
    