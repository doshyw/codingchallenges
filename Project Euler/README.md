# Project Euler

This repository consists of my write-ups/solutions to the code challenges set out by HackerRank's [Project Euler](https://www.hackerrank.com/contests/projecteuler/). For the most part, they are in Python, unless listed otherwise. The following outlines the completion status of the current set of challenges:

| No. | Challenge            | Completion Status |
|:---:|:--------------------:|:-----------------:|
|  1  | [Multiples of 3 and 5](https://gitlab.com/doshyw/project-euler/-/wikis/%231-Multiples-of-3-and-5) | Complete!         |
