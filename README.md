# Coding Challenges

This repository serves to capture my individual solutions to various online coding challenges, and as such are representative of solely my work. The following provides a brief outline of the solutions in this repository:

|          Name         | Challenge Link |
|:----------------------|:---------------|
| Advent of Code | https://adventofcode.com |
| LeetCode       | https://leetcode.com/problemset/all/ |
| Project Euler  | https://www.hackerrank.com/contests/projecteuler/challenges |
