import sys
from typing import Any

# Template functionality, currently mirrors input variable
def functionality(val: int) -> Any:
    return val

# Takes lines from stdin and operates on each one, printing to stdout
def main() -> None:
    for line in sys.stdin:
        val = int(line)
        ans = functionality(val)
        print(ans)

if __name__ == "__main__":
    main()