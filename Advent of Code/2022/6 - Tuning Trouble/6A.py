import sys
from typing import Any, Dict

# Template functionality, currently mirrors input variable
def functionality(val: int) -> Any:
    return val

# Takes lines from stdin and operates on each one, printing to stdout
def main() -> None:
    last_seen: Dict[str, int] = {}
    best_start = 0
    line = sys.stdin.readline()
    for current in range(len(line)):
        if current - best_start >= 4:
            print(best_start + 4)
            return
        
        if line[current] in last_seen and last_seen[line[current]] >= best_start:
            best_start = last_seen[line[current]] + 1
        
        last_seen[line[current]] = current

if __name__ == "__main__":
    main()