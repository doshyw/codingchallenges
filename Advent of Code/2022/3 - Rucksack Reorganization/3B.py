import sys
from typing import Any, List   

def get_priority(char: str):
    val = ord(char)
    if val < ord("a"):
        return val - ord("A") + 27
    else:
        return val - ord("a") + 1
            
def three_way_match(A: str, B: str, C: str):
    arr_A = [0] * len(A)
    arr_B = [0] * len(B)
    arr_C = [0] * len(C)
    
    for i in range(len(A)):
        arr_A[i] = get_priority(A[i])
    for i in range(len(B)):
        arr_B[i] = get_priority(B[i])
    for i in range(len(C)):
        arr_C[i] = get_priority(C[i])
        
    arr_A.sort()
    arr_B.sort()
    arr_C.sort()
    
    i = 0
    j = 0
    k = 0
    while True:
        if arr_A[i] == arr_B[j] and arr_A[i] == arr_C[k]:
            return arr_A[i]
        elif arr_A[i] <= arr_B[j]:
            if arr_A[i] <= arr_C[k]:
                i += 1
            else:
                k += 1
        else:
            if arr_B[j] <= arr_C[k]:
                j += 1
            else:
                k += 1

# Takes lines from stdin and operates on each one, printing to stdout
def main() -> None:
    sum = 0
    for line in sys.stdin:
        bag_1 = line.strip()
        bag_2 = sys.stdin.readline().strip()
        bag_3 = sys.stdin.readline().strip()
        sum += three_way_match(bag_1, bag_2, bag_3)
    print(sum)
        

if __name__ == "__main__":
    main()