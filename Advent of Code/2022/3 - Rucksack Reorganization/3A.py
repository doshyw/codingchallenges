import sys
from typing import Any

def get_priority(char: str):
    val = ord(char)
    if val < ord("a"):
        return val - ord("A") + 27
    else:
        return val - ord("a") + 1

# Template functionality, currently mirrors input variable
def sort_and_match(A: str, B: str) -> Any:
    arr_A = [0] * len(A)
    arr_B = [0] * len(A)
    
    for i in range(len(A)):
        arr_A[i] = get_priority(A[i])
        arr_B[i] = get_priority(B[i])
        
    arr_A.sort()
    arr_B.sort()
    
    i = 0
    j = 0
    while i < len(A) and j < len(A):
        if arr_A[i] == arr_B[j]:
            return arr_A[i]
        elif arr_A[i] < arr_B[j]:
            i += 1
        else:
            j += 1
    

# Takes lines from stdin and operates on each one, printing to stdout
def main() -> None:
    sum = 0
    for line in sys.stdin:
        area_1 = line[0:((len(line) - 1) // 2)]
        area_2 = line[((len(line) - 1) // 2):-1]
        sum += sort_and_match(area_1, area_2)
    print(sum)
        

if __name__ == "__main__":
    main()