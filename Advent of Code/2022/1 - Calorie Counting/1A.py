import sys
from typing import Any, List

# Template functionality, currently mirrors input variable
def functionality(val: int) -> Any:
    return val

# Takes lines from stdin and operates on each one, printing to stdout
def main() -> None:
    best_sum = -1
    elf_sum = 0
    for line in sys.stdin:
        if line == "\n":
            if elf_sum > best_sum:
                best_sum = elf_sum
            elf_sum = 0
        else:
            elf_sum += int(line)
            
    print(best_sum)

if __name__ == "__main__":
    main()