import sys
from typing import Any, List

N = 3

# Template functionality, currently mirrors input variable
def replace_in_list_sorted(arr: List[int], val: int) -> None:
    for i, x in enumerate(arr):
        if val > x:
            for j in range(i, len(arr)):
                temp = arr[j]
                arr[j] = val
                val = temp
            break

# Takes lines from stdin and operates on each one, printing to stdout
def main() -> None:
    best_sums = [-1] * N
    elf_sum = 0
    for line in sys.stdin:
        if line == "\n":
            if elf_sum > best_sums[N - 1]:
                replace_in_list_sorted(best_sums, elf_sum)
            elf_sum = 0
        else:
            elf_sum += int(line)
            
    print(best_sums)
    print(sum(best_sums))

if __name__ == "__main__":
    main()