import sys
from typing import Any

# Template functionality, currently mirrors input variable
def functionality(val: int) -> Any:
    return val

# Takes lines from stdin and operates on each one, printing to stdout
def main() -> None:
    
    line = sys.stdin.readline()
    num_stacks = (len(line) // 4)
    stacks = [[] for i in range(num_stacks)]
    while line[1] != "1":
        for i in range(num_stacks):
            if line[4*i + 1] != " ":
                stacks[i].append(line[4*i + 1])
        line = sys.stdin.readline()
    line = sys.stdin.readline()
    
    for line in sys.stdin:
        split_1 = line[5:].split(" from ")
        split_2 = split_1[1].split(" to ")
        num_moved = int(split_1[0])
        moved_from = int(split_2[0]) - 1
        moved_to = int(split_2[1]) - 1
        
        for i in range(num_moved):
            stacks[moved_to].insert(0, stacks[moved_from].pop(0))
    
    output = ""
    for i in range(num_stacks):
        output += stacks[i][0]
    print(output)

if __name__ == "__main__":
    main()