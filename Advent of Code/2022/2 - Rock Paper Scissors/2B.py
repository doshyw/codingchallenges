import sys
from typing import Any

DRAW_SCORE = 3
WIN_SCORE = 6

# Template functionality, currently mirrors input variable
def score(opp: int, outcome: int) -> Any:
    you = 1 + (opp + outcome) % 3
    
    if outcome == 2:
        return you + DRAW_SCORE
    elif outcome == 3:
        return you + WIN_SCORE
    else:
        return you
        

# Takes lines from stdin and operates on each one, printing to stdout
def main() -> None:
    total = 0
    for line in sys.stdin:
        opponent_choice = ord(line[0]) - ord("A") + 1
        player_choice   = ord(line[2]) - ord("X") + 1
        total += score(opponent_choice, player_choice)
    print(total)

if __name__ == "__main__":
    main()