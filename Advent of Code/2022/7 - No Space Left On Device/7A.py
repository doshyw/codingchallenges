import sys
from typing import Any, Dict, List, Optional, Tuple

SIZE_LIMIT = 100000

class FileNode:
    parent: Optional["FileNode"]
    children: Dict[str, "FileNode"]
    name: str
    size: int
    
    def recursive_size(self) -> int:
        size = self.size
        for c in self.children.items():
            if c[0] != "..":
                size += c[1].recursive_size()
        return size
    
    def sum_walk(self) -> Tuple[int, int]:
        if self.size == 0:
            dir_size = 0
            walked_size = 0
            for c in self.children.items():
                if c[0] != "..":
                    contents = c[1].sum_walk()
                    dir_size += contents[0]
                    walked_size += contents[1]
            if dir_size <= SIZE_LIMIT:
                walked_size += dir_size
            return (dir_size, walked_size)
        else:
            return (self.size, 0)
    
    def get_child(self, name: str):
        return self.children[name]
        
    def make_child(self, name: str, size: int):
        self.children[name] = FileNode(self, name, size)
    
    def __init__(self, parent: Optional["FileNode"], name: str, size: int):
        self.parent = parent
        if parent is None:
            self.children = {}
        else:
            self.children = {"..": parent}
        self.name = name
        self.size = size

# Takes lines from stdin and operates on each one, printing to stdout
def main() -> None:
    root = FileNode(None, "ROOT", 0)
    root.make_child("/", 0)
    current = root
    for line in sys.stdin:
        if line[0] == "$":
            cmd = line[2:-1].split(" ")
            if cmd[0] == "cd":
                current = current.get_child(cmd[1])
        else:
            file = line[:-1].split(" ")
            if file[0] == "dir":
                current.make_child(file[1], 0)
            else:
                current.make_child(file[1], int(file[0]))
    
    print(root.sum_walk()[1])

if __name__ == "__main__":
    main()