import sys
from typing import Any

# Template functionality, currently mirrors input variable
def check_overlap(A: int, B: int, C: int, D: int) -> Any:
    return (A >= C and B <= D) or (C >= A and D <= B)
        

# Takes lines from stdin and operates on each one, printing to stdout
def main() -> None:
    sum = 0
    for line in sys.stdin:
        sections = line.strip().split(",")
        bounds_A = sections[0].split("-")
        bounds_B = sections[1].split("-")
        start_A = int(bounds_A[0])
        end_A = int(bounds_A[1])
        start_B = int(bounds_B[0])
        end_B = int(bounds_B[1])
        if check_overlap(start_A, end_A, start_B, end_B):
            sum += 1
    print(sum)

if __name__ == "__main__":
    main()