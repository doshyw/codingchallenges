import sys

# Takes lines from stdin and operates on each one, printing to stdout
def main() -> None:
    global_ratings = []
    valid_oxygen = []
    valid_co2 = []
    
    line = sys.stdin.readline()
    num_bits = len(line) - 1
    bits = [2*int(x)-1 for x in line[:-1]]
    
    for line in sys.stdin:
        global_ratings.append(line[:-1])
        for i in range(num_bits):
            bits[i] += ((line[i] == '1') * 2 - 1)
    
    for i in range(len(global_ratings)):
        rating = global_ratings.pop(0)
        if((rating[0] == '1') == (bits[0] >= 0)):
            valid_oxygen.append(rating)
        else:
            valid_co2.append(rating)
         
    i = 1
    while(len(valid_oxygen) > 1 and i < num_bits):
        n = len(valid_oxygen)
        for _ in range(n):
            rating = valid_oxygen.pop(0)
            if((rating[i] == '1') == (bits[i] >= 0)):
                valid_oxygen.append(rating)
        i += 1
        
    if(len(valid_oxygen) == 0):
        oxygen_rating = rating
    else:
        oxygen_rating = valid_oxygen[0]
    
    i = 1
    while(len(valid_co2) > 1 and i < num_bits):
        n = len(valid_co2)
        for _ in range(n):
            rating = valid_co2.pop(0)
            if((rating[i] == '1') != (bits[i] >= 0)):
                valid_co2.append(rating)
        i += 1
        
    if(len(valid_co2) == 0):
        co2_rating = rating
    else:
        co2_rating = valid_co2[0]
    
    oxygen = 0
    co2 = 0
    for i in range(num_bits):
        oxygen *= 2
        co2 *= 2
        oxygen += (oxygen_rating[i] == '1')
        co2 += (co2_rating[i] == '1')
    
    print(oxygen * co2)
    

if __name__ == "__main__":
    main()