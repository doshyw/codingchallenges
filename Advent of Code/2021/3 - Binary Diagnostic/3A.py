import sys
from typing import Any

# Template functionality, currently mirrors input variable
def functionality(val: int) -> Any:
    return val

# Takes lines from stdin and operates on each one, printing to stdout
def main() -> None:
    line = sys.stdin.readline()
    num_bits = len(line) - 1
    bits = [2*int(x)-1 for x in line[:-1]]
    
    for line in sys.stdin:
        for i in range(num_bits):
            bits[i] += ((line[i] == '1') * 2 - 1)
    
    gamma = 0
    for i in range(num_bits):
        gamma *= 2
        gamma += (bits[i] > 0)
        
    epsilon = gamma ^ (pow(2, num_bits) - 1)
        
    print(gamma * epsilon) 

if __name__ == "__main__":
    main()