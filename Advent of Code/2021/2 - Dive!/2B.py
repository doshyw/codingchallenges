import sys
from typing import Any

# Takes lines from stdin and operates on each one, printing to stdout
def main() -> None:
    aim = 0
    x = 0
    y = 0
    
    for line in sys.stdin:
        length = len(line)
        # forward
        if(length == 10):
            val = int(line[8:-1])
            x += val
            y += aim*val
        # down
        elif(length == 7):
            aim += int(line[5:-1])
        # up
        elif(length == 5):
            aim -= int(line[3:-1])
            
    print(x*y)

if __name__ == "__main__":
    main()