import sys
from typing import Any

# Takes lines from stdin and operates on each one, printing to stdout
def main() -> None:
    x = 0
    y = 0
    
    for line in sys.stdin:
        length = len(line)
        # forward
        if(length == 10):
            x += int(line[8:-1])
        # down
        elif(length == 7):
            y += int(line[5:-1])
        # up
        elif(length == 5):
            y -= int(line[3:-1])
            
    print(x*y)

if __name__ == "__main__":
    main()