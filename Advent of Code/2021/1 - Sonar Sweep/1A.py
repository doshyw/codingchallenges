import sys

# Takes lines from stdin and operates on each one, printing to stdout
def main():
    total = 0
    previous = int(sys.stdin.readline())
    
    for line in sys.stdin:
        val = int(line)
        if(val > previous):
            total += 1
        previous = val
    
    print(total)

if __name__ == "__main__":
    main()