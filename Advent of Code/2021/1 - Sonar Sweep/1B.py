import sys

# Takes lines from stdin and operates on each one, printing to stdout
def main():
    window_size = 3
    buffer = [-1] * window_size
    
    total = 0
    
    for i in range(window_size, 0, -1):
        buffer[i - 1] = int(sys.stdin.readline())
        
    current_sum = sum(buffer)
    previous = current_sum
        
    for line in sys.stdin:
        val = int(line)
        
        current_sum -= buffer.pop()
        current_sum += val
        buffer.insert(0, val)
        
        if(current_sum > previous):
            total += 1
        previous = current_sum
        
    print(total)

if __name__ == "__main__":
    main()