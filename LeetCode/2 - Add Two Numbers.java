/*
	https://leetcode.com/problems/add-two-numbers/
	You are given two non-empty linked lists representing two non-negative integers. The digits are stored in reverse order, and each of their nodes contains a single digit. Add the two numbers and return the sum as a linked list.
	You may assume the two numbers do not contain any leading zero, except the number 0 itself.
	
	Constraints:
	The number of nodes in each linked list is in the range [1, 100].
	0 <= Node.val <= 9
	It is guaranteed that the list represents a number that does not have leading zeros.
*/

public class Runner {
    public static void main(String[] args) {
        Solution solution = new Solution();
        ListNode num1 = ListNode.getNum(399);
        ListNode num2 = ListNode.getNum(16);
        num1.printVal();
        num2.printVal();
        solution.addTwoNumbers(num1, num2).printVal();
    }
}

class ListNode {
    int val;
    ListNode next;
    ListNode() {}
    ListNode(int val) { this.val = val; }
    ListNode(int val, ListNode next) { this.val = val; this.next = next; }
    void printVal() {
        ListNode n = this;
        String output = "";
        while(n != null) {
            output = Integer.toString(n.val).concat(output);
            n = n.next;
        }
        System.out.println(output);
    }
    static ListNode getNum(int num) {
        ListNode me = new ListNode(num % 10);
        num /= 10;
        ListNode n = me;
        while(num > 0) {
            n.next = new ListNode(num % 10);
            num /= 10;
            n = n.next;
        }
        return me;
    }
}

class Solution {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        
        // Initialise smallest digit
        int sum = l1.val + l2.val;
        int carry = sum / 10;
        ListNode output = new ListNode(sum % 10);
        ListNode traverse = output;
        l1 = l1.next;
        l2 = l2.next;
        
        // Traverse the addend lists equally
        while(l1 != null && l2 != null) {
            // Assign remainder and carry
            sum = l1.val + l2.val + carry;
            carry = sum / 10;
            traverse.next = new ListNode(sum % 10);
            
            // Move to next digit
            traverse = traverse.next;
            l1 = l1.next;
            l2 = l2.next;
        }
        
        // Take remaining digits from longer number
        if(l2 != null) l1 = l2;
        while(l1 != null) {
            // Take all of the remaining digits if no remaining carries
            if(carry == 0)
            {
                traverse.next = l1;
                break;
            }
            
            // Handle remaining carries
            sum = l1.val + carry;
            carry = sum / 10;
            traverse.next = new ListNode(sum % 10);
            traverse = traverse.next;
            l1 = l1.next;
        }
        
        if(carry == 1)
        {
            traverse.next = new ListNode(1);
        }
        
        return output;
    }
}