/*
    https://leetcode.com/problems/longest-palindromic-substring/
    Given a string s, return the longest palindromic substring in s.
    A string is called a palindrome string if the reverse of that string is the same as the original string.
    
   Constraints:
   1 <= s.length <= 1000
   s consist of only digits and English letters.
*/

public class Runner {
    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.longestPalindrome("asdfxxfrty"));
    }
}

class Solution {
    // Calculate longest palindrome around one or two character pivot
    private int maxPalindromeAtPivot(String s, int pivot1, int pivot2) {
        while(pivot1 > -1 && pivot2 < s.length() && s.charAt(pivot1) == s.charAt(pivot2))
        {
            pivot1--;
            pivot2++;
        }
        return pivot2 - pivot1 - 1;
    }
    
    public String longestPalindrome(String s) {
        int maxVal = 0;
        int start = 0;
        int returnVal = -1;
        // Check all pivots for their maximum palindrome
        for(int i = 0; i < s.length(); i++)
        {
            // Odd length palindrome
            returnVal = maxPalindromeAtPivot(s, i, i);
            if(returnVal > maxVal) {
                maxVal = returnVal;
                start = i - returnVal / 2;
            }
            
            // Even length palindrome
            returnVal = maxPalindromeAtPivot(s, i, i + 1);
            if(returnVal > maxVal) {
                maxVal = returnVal;
                start = i - returnVal / 2 + 1;
            }
        }
        return s.substring(start, start + maxVal);
    }
}