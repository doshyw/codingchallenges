/*
    https://leetcode.com/problems/median-of-two-sorted-arrays/
    Given two sorted arrays "nums1" and "nums2" of size m and n respectively, return the median of the two sorted arrays.
    The overall run time complexity should be O(log (m+n)).
    
   Constraints:
    nums1.length == m
    nums2.length == n
    0 <= m <= 1000
    0 <= n <= 1000
    1 <= m + n <= 2000
    -10**6 <= nums1[i], nums2[i] <= 10**6
*/

import java.util.HashMap;

public class Runner {
    public static void main(String[] args) {
        Solution solution = new Solution();
        int[] nums1 = {1, 2}, nums2 = {3, 4};
        System.out.println(solution.findMedianSortedArrays(nums1, nums2));
    }
}

class Solution {
    // Get the kth smallest element contained in either of two arrays by recursion
    private double getKthSmallest(int[] A, int[] B, int startA, int startB, int k)
    {
        // Edge cases where all elements of an array have been reserved
        if(startA > A.length - 1) return B[startB + k - 1];
        if(startB > B.length - 1) return A[startA + k - 1];
        
        // Default case for recursion, taking the smallest unreserved element
        if(k == 1) return Math.min(A[startA], B[startB]);
        
        // Calculate (k/2)th smallest element from each array, if there are k/2 elements or more
        int medianA = Integer.MAX_VALUE;
        int medianB = Integer.MAX_VALUE;
        if(startA + k / 2 - 1 < A.length) medianA = A[startA + k / 2 - 1];
        if(startB + k / 2 - 1 < B.length) medianB = B[startB + k / 2 - 1];
        
        // Recursively call getKthSmallest after reserving k/2 elements from the smallest k
        if(medianA < medianB) return getKthSmallest(A, B, startA + k/2, startB, k - k/2);
        else return getKthSmallest(A, B, startA, startB + k/2, k - k/2);
    }
    
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int m = nums1.length;
        int n = nums2.length;
        
        if((m + n) % 2 == 0)
        {
            return (getKthSmallest(nums1, nums2, 0, 0, (m + n) / 2)
                    + getKthSmallest(nums1, nums2, 0, 0, 1 + (m + n) / 2)) / 2.0;
        }
        else
        {
            return getKthSmallest(nums1, nums2, 0, 0, 1 + (m + n) / 2);
        }
        
    }
}