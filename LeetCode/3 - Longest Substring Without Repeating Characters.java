/*
    https://leetcode.com/problems/longest-substring-without-repeating-characters/
    Given a string s, find the length of the longest substring without repeating characters.
    
    Constraints:
    0 <= s.length <= 5 * 10**4
    s consists of English letters, digits, symbols and spaces.
*/

import java.util.HashMap;

public class Runner {
    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.lengthOfLongestSubstring("bbtablud"));
    }
}

class Solution {
    public int lengthOfLongestSubstring(String s) {
        int start = 0, end = 0, bestLength = 0, diff = 0;
        HashMap<Character, Integer> hm = new HashMap<>();
        char current;
        while(end < s.length())
        {
            current = s.charAt(end);
            if(hm.containsKey(current))
            {
                for(int i = start; i < hm.get(current); i++)
                {
                    hm.remove(s.charAt(i));
                }
                start = hm.get(current) + 1;
                hm.put(current, end);
            }
            else
            {
                hm.put(current, end);
                diff = end - start + 1;
                if(diff > bestLength)
                {
                    bestLength = diff;
                }
            }
            end++;
            
        }
        return bestLength;
    }
}