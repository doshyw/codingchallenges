/*
    https://leetcode.com/problems/reverse-integer/
    Given a signed 32-bit integer x, return x with its digits reversed. If reversing x causes the value to go outside the signed 32-bit integer range [-2**31, 2**31 - 1], then return 0.
	Assume the environment does not allow you to store 64-bit integers (signed or unsigned).
	
    Constraints:
    -2**31 <= x <= 2**31 - 1
*/

public class Runner {
    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.reverse(-1463847412));
    }
}

class Solution {
    public int reverse(int x) {
        // Transform all of the digits except for the last one
        int output = 0;
        while(x > 9 || x < -9)
        {
            output = output * 10 + x % 10;
            x /= 10;
        }
        
        // Remove edge case where final number is out of bounds
        if(output > Integer.MAX_VALUE / 10 || output < Integer.MIN_VALUE / 10) return 0;
        
        // Transform final digit and return
        output = output * 10 + x;
        return output;
    }
}