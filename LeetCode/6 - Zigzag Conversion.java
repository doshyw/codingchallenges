/*
    https://leetcode.com/problems/zigzag-conversion/
    The string "PAYPALISHIRING" is written in a zigzag pattern on a given number of rows like this:
    P   A   H   N
    A P L S I I G
    Y   I   R
    And then read line by line: "PAHNAPLSIIGYIR"
    Write the code that will take a string and make this conversion given a number of rows:
    string convert(string s, int numRows);
    
   Constraints:
    1 <= s.length <= 1000
    s consists of English letters (lower-case and upper-case), ',' and '.'.
    1 <= numRows <= 1000
*/

import java.util.ArrayList;
import java.util.HashMap;

public class Runner {
    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.convert("PAYPALISHIRING", 3));
    }
}

class Solution {
    public String convert(String s, int numRows) {
        // Eliminate trivial cases where input == output
        if(s.length() <= numRows || numRows == 1) return s;
        
        char[] output = new char[s.length()];
        int x = 0;
        int N = s.length();
        int cycleLength = 2*numRows - 2;
        
        // Iterate over the first row
        int pos = 0;
        while(pos < N) {
            output[x++] = s.charAt(pos);
            pos += cycleLength;
        }
        
        // Take the middle set of rows, noting that there are two characters per cycle,
        //   offset inwards i positions from each start point of the cycle
        for(int i = 1; i < numRows - 1; i++)
        {
            pos = 0;
            while(pos + i < N) {
                output[x++] = s.charAt(pos + i);
                if(pos + cycleLength - i < N) output[x++] = s.charAt(pos + cycleLength - i);
                pos += cycleLength;
            }
        }
        
        // Iterate over the final row
        pos = cycleLength / 2;
        while(pos < N) {
            output[x++] = s.charAt(pos);
            pos += cycleLength;
        }
        
        return new String(output);
    }
}