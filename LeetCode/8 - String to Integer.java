/*
    https://leetcode.com/problems/string-to-integer-atoi/
    Implement the myAtoi(string s) function, which converts a string to a 32-bit signed integer (similar to C/C++'s atoi function).

    The algorithm for myAtoi(string s) is as follows:
    1. Read in and ignore any leading whitespace.
    2. Check if the next character (if not already at the end of the string) is '-' or '+'. Read this character in if it is either. This determines if the final result is negative or positive respectively. Assume the result is positive if neither is present.
    3. Read in next the characters until the next non-digit character or the end of the input is reached. The rest of the string is ignored.
    4. Convert these digits into an integer (i.e. "123" -> 123, "0032" -> 32). If no digits were read, then the integer is 0. Change the sign as necessary (from step 2).
    5. If the integer is out of the 32-bit signed integer range [-2**31, 2**31 - 1], then clamp the integer so that it remains in the range. Specifically, integers less than -2**31 should be clamped to -2**31, and integers greater than 2**31 - 1 should be clamped to 2**31 - 1.
    6. Return the integer as the final result.
    
    Note:
    Only the space character ' ' is considered a whitespace character.
    Do not ignore any characters other than the leading whitespace or the rest of the string after the digits.
    
    Constraints:
    0 <= s.length <= 200
    s consists of English letters (lower-case and upper-case), digits (0-9), ' ', '+', '-', and '.'.
*/

public class Runner {
    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.myAtoi("-91283472332"));
    }
}

class Solution {
    public int myAtoi(String s) {
        // Remove leading whitespace and return zero for empty string
        int pointer = 0;
        while(pointer < s.length() && s.charAt(pointer) == ' ') pointer++;
        if(pointer == s.length()) return 0;
        
        // Take and store a negative sign, if any
        boolean isNegative = false;
        if(s.charAt(pointer) == '-') {
            isNegative = true;
            pointer++;
        }
        else if(s.charAt(pointer) == '+') {
            pointer++;
        }
        
        // Take digits sequentially, clamping the value by checking to see
        //   if adding a new digit will overflow
        int number = 0;
        while(pointer < s.length() && s.charAt(pointer) >= '0' && s.charAt(pointer) <= '9')
        {
            if(number > Integer.MAX_VALUE / 10) {
                if(!isNegative) return Integer.MAX_VALUE;
                                return Integer.MIN_VALUE;
            }
            else if(number == Integer.MAX_VALUE / 10)
            {
                if(!isNegative && s.charAt(pointer) - '0' > 6) return Integer.MAX_VALUE;
                if( isNegative && s.charAt(pointer) - '0' > 7) return Integer.MIN_VALUE;
            }
            number = number * 10 + (s.charAt(pointer) - '0');
            pointer++;
        }
        
        // Return the number with the proper sign
        if(isNegative) return -number;
        return number;
    }
}