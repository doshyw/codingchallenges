/*
	https://leetcode.com/problems/two-sum/
	Given an array of integers "nums" and an integer "target", return indices of the two numbers such that they add up to "target".
	You may assume that each input would have exactly one solution, and you may not use the same element twice.
	You can return the answer in any order.
	
	Constraints:
	2 <= nums.length <= 10**4
	-10**9 <= nums[i] <= 10**9
	-10**9 <= target <= 10**9
	Only one valid answer exists.
*/

import java.util.HashMap;

public class Runner {
    public static void main(String[] args) {
        Solution solution = new Solution();
        int[] nums = {2,7,11,15};
        int target = 9;
        System.out.println(Arrays.toString(solution.twoSum(nums, target)));
    }
}

class Solution {
    public int[] twoSum(int[] nums, int target) {
        HashMap<Integer, Integer> hm = new HashMap<>();
        int diff;
        for(int i = 0; i < nums.length; i++)
        {
            diff = target - nums[i];
            if(hm.containsKey(diff))
            {
                return new int[]{hm.get(diff), i};
            }
            hm.put(nums[i], i);
        }
        return null;
    }
}