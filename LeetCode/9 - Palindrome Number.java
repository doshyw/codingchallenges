/*
    https://leetcode.com/problems/palindrome-number/
    Given an integer x, return true if x is palindrome integer.
    An integer is a palindrome when it reads the same backward as forward.
    For example, 121 is a palindrome while -121 is not.
    
    Constraints:
    -2**31 <= x <= 2**31 - 1
*/

public class Runner {
    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.isPalindrome(120));
    }
}

class Solution {
    public boolean isPalindrome(int x) {
        if(x < 0) return false;
        
        int temp = x;
        int y = 0;
        while(temp > 0)
        {
            y = y * 10 + temp % 10;
            temp /= 10;
        }
        return x == y;
    }
}